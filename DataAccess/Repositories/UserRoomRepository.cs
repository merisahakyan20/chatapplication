﻿using Core.Database;
using Core.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories
{
    public class UserRoomRepository : RepositoryBase<UsersRoom>, IUserRoomRepository
    {
        public UserRoomRepository(ChatDbContext context) : base(context)
        {
        }
    }
}
