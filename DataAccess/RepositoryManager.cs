﻿using Core;
using Core.Database;
using Core.RepositoryInterfaces;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class RepositoryManager : IRepositoryManager
    {
        private ChatDbContext _context;
        public RepositoryManager(ChatDbContext context)
        {
            _context = context;
        }
        private IUserRepository _users;
        public IUserRepository Users => _users = new UserRepository(_context);

        private IRoomRepository _rooms;
        public IRoomRepository Rooms => _rooms = new RoomRepository(_context);

        private IMessageRepository _messages;
        public IMessageRepository Messages => _messages = new MessageRepository(_context);

        private IUserRoomRepository _userRooms;
        public IUserRoomRepository UserRooms => _userRooms = new UserRoomRepository(_context);

    }
}
