using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Operations;
using Core;
using Core.Database;
using Core.OperationInterfaces;
using Core.RepositoryInterfaces;
using DataAccess;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PresentationLayer.Hubs;

namespace PresentationLayer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        //public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
            });

            services.AddSingleton(typeof(IUserOperations), typeof(UserOperations));
            services.AddSingleton(typeof(IRoomOperations), typeof(RoomOperations));
            services.AddSingleton(typeof(IMessageOperations), typeof(MessageOperations));

            services.AddSingleton(typeof(IRepositoryManager), typeof(RepositoryManager));
            services.AddSingleton(typeof(IUserRepository), typeof(UserRepository));
            services.AddSingleton(typeof(IRoomRepository), typeof(RoomRepository));
            services.AddSingleton(typeof(IUserRoomRepository), typeof(UserRoomRepository));
            services.AddSingleton(typeof(IMessageRepository), typeof(MessageRepository));

            var connection = ConfigurationExtensions.GetConnectionString(this.Configuration, "DefaultConnection");
            services.AddTransient<ChatDbContext>(o => new ChatDbContext(connection));

            services.AddRazorPages()
                .AddNewtonsoftJson();

            services.AddMvc();

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseCookiePolicy();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });


            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chatHub");
            });
        }
    }
}
