﻿using Core.Database;
using Core.OperationInterfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PresentationLayer.Hubs
{
    public class ChatHub : Hub
    {
        IRoomOperations _rooms;
        IMessageOperations _messages;
        IUserOperations _users;
        public ChatHub(IRoomOperations roomOperations, IMessageOperations messageOperations, IUserOperations userOperations)
        {
            _rooms = roomOperations;
            _messages = messageOperations;
            _users = userOperations;
        }
        public async Task CreateRoom(string name)
        {
            var room = await _rooms.CreateRoomAsync(name);
            await Clients.All.SendAsync("createRoom", name, room.Id);
        }

        public async Task JoinRoom(int roomId, int userId)
        {
            await _rooms.JoinRoomAsync(userId, roomId);
            var room = await _rooms.GetRoomByIdAsync(roomId);
            await Groups.AddToGroupAsync(Context.ConnectionId, room.Name);
        }

        public async Task LeaveRoom(int roomId, int userId)
        {
            await _rooms.LeaveRoomAsync(userId, roomId);
            var room = await _rooms.GetRoomByIdAsync(roomId);

            await Groups.RemoveFromGroupAsync(Context.ConnectionId, room.Name);
        }
        public async Task SendMessageToRoom(int userId, string message, int roomId)
        {
            var user = await _users.GetUserByIdAsync(userId);
            await _messages.AddMessageAsync(roomId, userId, message);

            await Clients.All.SendAsync("Send", roomId, user.UserName, message);
        }

        public async Task<IEnumerable<Message>> GetRoomMessages(int roomId)
        {
            return await _messages.GetRoomMessagesAsync(roomId);
        }
    }
}
