﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Database
{
    public class UsersRoom : BaseEntity
    {
        public int RoomId { get; set; }
        public int UserId { get; set; }
    }
}
