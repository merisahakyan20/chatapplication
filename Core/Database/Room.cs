﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Database
{
    public class Room:BaseEntity
    {
        public Room()
        {
            this.Messages = new HashSet<Message>();
        }

        public string Name { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
