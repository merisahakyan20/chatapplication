﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Database
{
    public class User: BaseEntity
    {
        public User()
        {
            this.Messages = new HashSet<Message>();
        }

        public string UserName { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
         ErrorMessage = "Characters are not allowed.")]
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
