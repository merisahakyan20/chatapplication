﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Database
{
    public class ChatDbContext : DbContext
    {
        string connection = "";
        public ChatDbContext(string connection)
        {
            this.connection = connection;
        }

        public DbSet<Message> Messages { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<UsersRoom> UserRooms { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                //optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ChatDb;Trusted_Connection=True;ConnectRetryCount=0");
                optionsBuilder.UseSqlServer(connection);
        }
    }

    public class ApplicationContextFactory : IDesignTimeDbContextFactory<ChatDbContext>
    {
        public ChatDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ChatDbContext>();
            builder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ChatDb;Trusted_Connection=True;ConnectRetryCount=0");
            return new ChatDbContext("Server=(localdb)\\mssqllocaldb;Database=ChatDb;Trusted_Connection=True;ConnectRetryCount=0");
        }
    }
}
