﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Database
{
    public class Message: BaseEntity
    {
        
        [Required]
        public int UserId { get; set; }
        [Required]
        public int RoomId { get; set; }
        [Required]
        public string MessageText { get; set; }

        public System.DateTime DateTime { get; set; }
        [Required]
        public bool Edited { get; set; }

        public virtual Room Room { get; set; }
        public virtual User User { get; set; }
    }
}
