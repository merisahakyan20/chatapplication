﻿using Core.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public interface IRepositoryManager
    {
        IMessageRepository Messages { get; }
        IUserRepository Users { get; }
        IRoomRepository Rooms { get; }
        IUserRoomRepository UserRooms { get; }
    }
}
