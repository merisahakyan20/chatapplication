﻿using Core.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.OperationInterfaces
{
    public interface IRoomOperations
    {
        Task<Room> CreateRoomAsync(string name);
        Task JoinRoomAsync(int userId, int roomId);
        Task LeaveRoomAsync(int userId, int roomId);
        Task<Room> GetRoomByIdAsync(int roomId);
    }
}
