﻿using Core.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.OperationInterfaces
{
    public interface IMessageOperations
    {
        Task AddMessageAsync(int roomId, int userId, string message);
        Task<IEnumerable<Message>> GetRoomMessagesAsync(int roomId);
    }
}
