﻿using Core.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.OperationInterfaces
{
    public interface IUserOperations
    {
        Task<User> GetUserByIdAsync(int id);
        Task<IEnumerable<User>> GetRoomUsersAsync(int roomId);
        
    }
}
