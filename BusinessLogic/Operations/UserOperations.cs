﻿using Core;
using Core.Database;
using Core.OperationInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Operations
{
    public class UserOperations : IUserOperations
    {
        IRepositoryManager _repositoryManager;
        public UserOperations(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }
        public async Task<IEnumerable<User>> GetRoomUsersAsync(int roomId)
        {
            var userIds = await _repositoryManager.UserRooms.GetAll().Where(ur => ur.RoomId == roomId).Select(ur => ur.UserId).ToListAsync();

            var users = await _repositoryManager.Users.GetAll().Where(u => userIds.Contains(u.Id)).ToListAsync();

            return users;
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            return await _repositoryManager.Users.GetSingleAsync(id);
        }
    }
}
