﻿using Core;
using Core.Database;
using Core.OperationInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Operations
{
    public class MessageOperations : IMessageOperations
    {
        IRepositoryManager _repositoryManager;
        public MessageOperations(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }
        public async Task AddMessageAsync(int roomId, int userId, string message)
        {
            _repositoryManager.Messages.Add(new Message
            {
                DateTime = DateTime.Now,
                MessageText = message,
                RoomId = roomId,
                UserId = userId,
                Edited = false,
            });

            await _repositoryManager.Messages.SaveChangesAsync();
        }

        public async Task<IEnumerable<Message>> GetRoomMessagesAsync(int roomId)
        {
            return await _repositoryManager.Messages.GetAll().Where(m => m.RoomId == roomId).ToListAsync();
        }
    }
}
