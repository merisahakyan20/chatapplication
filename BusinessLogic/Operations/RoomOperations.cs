﻿using Core;
using Core.Database;
using Core.OperationInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Operations
{
    public class RoomOperations : IRoomOperations
    {
        IRepositoryManager _repositoryManager;
        public RoomOperations(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }
        public async Task<Room> CreateRoomAsync(string name)
        {
            var room = new Room
            {
                Name = name
            };
            _repositoryManager.Rooms.Add(room);
            await _repositoryManager.Rooms.SaveChangesAsync();

            return room;
        }

        public async Task<Room> GetRoomByIdAsync(int roomId)
        {
            return await _repositoryManager.Rooms.GetSingleAsync(roomId);
        }

        public async Task JoinRoomAsync(int userId, int roomId)
        {
            _repositoryManager.UserRooms.Add(new UsersRoom
            {
                RoomId = roomId,
                UserId = userId
            });
            await _repositoryManager.UserRooms.SaveChangesAsync();
        }

        public async Task LeaveRoomAsync(int userId, int roomId)
        {
            var userRoom = _repositoryManager.UserRooms.GetAll().FirstOrDefault(ur => ur.RoomId == roomId && ur.UserId == userId);
            _repositoryManager.UserRooms.Delete(userRoom.Id);

            await _repositoryManager.UserRooms.SaveChangesAsync();
        }
    }
}
